#!/bin/bash
rm handout*
ghostscript -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=handout_screen.pdf main.pdf
zip -9 handout.zip handout_screen.pdf fibonacci.inc.tex herringbone.inc.tex lonestar.inc.tex bedsheet.inc.tex homework.inc.tex grundlagen.inc.tex main.tex

